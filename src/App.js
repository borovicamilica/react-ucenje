import React from 'react';
import logo from './logo.svg';
import './App.css';
import Great from './components/great'
import Welcome from './components/welcome'
import Hello from './components/Hello'
import Message from './components/Message'
import Counter from './components/Counter'
import EventBind from './components/EventBind';
import ParentComponents from './components/ParentComponents';
import UserGreating from './components/UserGreating';

function App() {
  
  return (
    <div className="App">
      <UserGreating />
      {/*<ParentComponents />
      <EventBind />*/}
      {/*<Counter />
       <Message />*/}
     {/* <Great name= "Bruce" heroName= "Betmen">
        <p>This is children props</p>
      </Great>
      <Great name= "Clark" heroName= "Supermen" >
        <button>Action</button>
      </Great>
      <Great name= "Diana" heroName= "SuperWomen" />
      <Welcome name= "Bruce" heroName= "Betmen" />
      <Welcome name= "Clark" heroName= "Supermen" />
      <Welcome name= "Diana" heroName= "SuperWomen" />
      <Hello />*/}
      
    </div>
  );
 
}

export default App;
